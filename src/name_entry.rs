mod name_entry {
    use std::io;
    fn name() {
        // Ask the user for a name
        println!("Please enter your name:");

        // Read the user's input and store it in a variable
        let mut name = String::new();
        io::stdin().read_line(&mut name).expect("Failed to read line");

        // Print out a greeting
        println!("Hello, {}!", name.trim());
    }
}