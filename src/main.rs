extern crate minifb;
extern crate rand;

use minifb::{Key, Window, WindowOptions};
use rand::{thread_rng, Rng};

const WIDTH: i32 = 640;
const HEIGHT: i32 = 480;

struct Star {
    x: f32,
    y: f32,
    angle: f32,
    old_angle: f32,
    trail_angle: f32,
}
const NUM_STARS: usize = 6000;
const DEFAULT_STAR: Star = Star {
    x: 0.0,
    y: 0.0,
    angle: 0.0,
    old_angle: 0.0,
    trail_angle: 0.0,
};

fn plot_pixel(buff: &mut Vec<u32>, x: i32, y: i32, color: u32) {
    if x < 0 {
        return;
    }
    if y < 0 {
        return;
    }
    if x >= WIDTH {
        return;
    }
    if y >= HEIGHT {
        return;
    }

    let index = (y * WIDTH + x) as usize;
    buff[index] = color;
}

fn draw_line(buff: &mut Vec<u32>, x1: i32, y1: i32, x2: i32, y2: i32, color: u32) {
    //println!("draw_line({},{},{},{})",x1,y1,x2,y2 );
    if (x1 == x2) && (y1 == y2) {
        plot_pixel(buff, x1, y1, color)
    } else {
        let dx = x2 - x1;
        let dy = y2 - y1;
        draw_line(buff, x1, y1, x1 + dx / 2, y1 + dy / 2, color);
        draw_line(buff, x2 - dx / 2, y2 - dy / 2, x2, y2, color);
    }
}

fn plot_big_pixel(buff: &mut Vec<u32>, x: i32, y: i32, color: u32) {
    plot_pixel(buff, x - 1, y - 1, color);
    plot_pixel(buff, x, y - 1, color);
    plot_pixel(buff, x, y, color);
    plot_pixel(buff, x - 1, y, color);
}

fn main() {
    let mut stars: [Star; NUM_STARS] = [DEFAULT_STAR; NUM_STARS];

    let mut rng = thread_rng();

    // Generate stars in a random order
    for i in 0..stars.len() {
        stars[i].x = rng.gen::<f32>() * 2.0 - 1.0;
        stars[i].y = rng.gen::<f32>() * 2.0 - 1.0;
        stars[i].angle = rng.gen::<f32>() * 360.0;
    }

    let mut buffer: Vec<u32> = vec![0; WIDTH as usize * HEIGHT as usize];
    let mut window = Window::new(
        "Test - ESC to exit",
        WIDTH as usize,
        HEIGHT as usize,
        WindowOptions::default(),
    )
    .unwrap_or_else(|e| {
        panic!("{}", e);
    });

    // Limit to max ~60 fps update rate
    window.limit_update_rate(Some(std::time::Duration::from_micros(16600)));

    let height = HEIGHT as f32;
    let width = WIDTH as f32;

    let mut zoom: f32 = 1.0;
    let mut zoom_direction = true;

    while window.is_open() && !window.is_key_down(Key::Escape) {
        for i in buffer.iter_mut() {
            *i = 0; // Black
        }

        if zoom_direction {
            zoom += 0.01;
            if zoom > 3.0 {
                zoom_direction = false
            }
        } else {
            zoom -= 0.01;
            if zoom < 0.4 {
                zoom_direction = true
            }
        }

        for i in 0..stars.len() {
            stars[i].trail_angle = stars[i].old_angle;
            stars[i].old_angle = stars[i].angle;
            stars[i].angle += 0.002;

            // Convert star coordinates to 3D coodrinates
            let a1 = stars[i].angle * ((1.0 / (stars[i].x)) + stars[i].y).abs();
            let x1 = zoom * stars[i].x * a1.cos() * width + width / 2.0;
            let y1 = zoom * stars[i].x * a1.sin() * height + height / 2.0;

            let a2 = stars[i].old_angle * ((1.0 / (stars[i].x)) + stars[i].y).abs();
            let x2 = zoom * stars[i].x * a2.cos() * width + width / 2.0;
            let y2 = zoom * stars[i].x * a2.sin() * height + height / 2.0;

            let a3 = stars[i].trail_angle * ((1.0 / (stars[i].x)) + stars[i].y).abs();
            let x3 = zoom * stars[i].x * a3.cos() * width + width / 2.0;
            let y3 = zoom * stars[i].x * a3.sin() * height + height / 2.0;
            //let z = stars[i].y;

            // Convert 3D coordinates to screen coordinates. Until then, draw in 2D
            // TODO let screenx =

            // Draw
            draw_line(
                &mut buffer,
                x2 as i32,
                y2 as i32,
                x3 as i32,
                y3 as i32,
                0x666666,
            );
            draw_line(
                &mut buffer,
                x1 as i32,
                y1 as i32,
                x2 as i32,
                y2 as i32,
                0xaaaaaa,
            );
            plot_big_pixel(&mut buffer, x1 as i32, y1 as i32, 0xffffff);
        }

        // We unwrap here as we want this code to exit if it fails. Real applications may want to handle this in a different way
        window
            .update_with_buffer(&buffer, WIDTH as usize, HEIGHT as usize)
            .unwrap();
    }
}
